const express = require('express');
const app = express();
const DB  = require('./config/database');
const userRouter = require('./api/users/user.router');
require("dotenv").config();

DB();
//Connecting with DB
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use("/api/users", userRouter);

app.get("/api", (req, res) => {
    res.json({
        success : 1,
        message : "This is rest apis working"
    })
});

app.listen(process.env.PORT, () => {
    console.log("Server up and Running on PORT : "+ process.env.PORT);
});