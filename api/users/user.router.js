const userController = require('./user.controller');
const router = require('express').Router();
const checkToken = require('../../auth/token_validation');

router.post('/adduser', checkToken.checkToken, userController.createUser);

router.get('/showall', checkToken.checkToken, userController.show_all);

router.get('/showone/:id', checkToken.checkToken, userController.show_one);

router.put('/update', checkToken.checkToken, userController.update);

router.delete('/delete', checkToken.checkToken, userController.delete);

router.post('/login', userController.login);

module.exports = router;