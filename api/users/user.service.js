const userModel = require('./user.model');

module.exports = {
    create : async (data) => {
        try {
            let userAdd = await userModel.create(data);
            if(userAdd == null) throw {message : `Something Went Wrong`};
            return {
                success: true,
                message: 'Data Added Successfully',
                error: null,
                user: userAdd
            };
        }
        catch (error) {
            return {
                success: false,
                message: 'Failed to Add User Details',
                error: error.message
            };
        }
    },

    getUsers : async () => {
        try {
            let userList = await userModel.find();
            if (userList == null) throw {message: `User List is empty`};
            return {
                success: true,
                message: 'ALL USERS GET SUCCESSFULLY',
                error: null,
                users: userList
            };
        }
        catch (error){
            return {
                success: false,
                message: 'Failed to Get All User',
                error: error.message
            };
        }
    },

    getUserById : async (id) => {
        try {
            let oneUser = await userModel.findOne({_id : id});
            if(oneUser == null) throw {message : `User ID is Not Found`};
            return {
                success: true,
                message: 'User ID Found',
                error: null,
                oneuser: oneUser
            };
        }
        catch (error){
            return {
                success: false,
                message: 'Failed to Get User Details',
                error: error.message
            };
        }
    },

    update : async (id,data) => {
        try {
            let userUpdate = await userModel.findByIdAndUpdate(id, data);
            if(userUpdate == null) throw {message: `User Details Not Found`};
            return {
                success: true,
                message: 'User ID Found',
                error: null,
                userupdate: userUpdate
            };
        }
        catch (error) {
            return {
                success: false,
                message: 'Failed to Update User Details',
                error: error.message
            };
        }
    },

    delete : async id => {
        try {
            let userDelete = await userModel.deleteOne({_id : id});
            if(userDelete == null) throw {message: `User ID Not Found`};
            return {
                success: true,
                message: 'User ID Found',
                error: null,
                userdelete: userDelete
            };
        }
        catch (error) {
            return {
                success: false,
                message: 'Failed to Delete User Details',
                error: error.message
            };
        }
    },

    getUserByUserEmail : async email => {
        return await userModel.findOne({email : email});
    }
};