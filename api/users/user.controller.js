const userService = require('./user.service');
const bcrypt = require('bcrypt');
const jsonwebtoken = require('jsonwebtoken');

module.exports = {
    createUser : (req, res) => {
        const body = req.body;
        const salt = bcrypt.genSaltSync(10);
        body.password = bcrypt.hashSync(body.password, salt);

        userService.create(body).then(result => {
            if(result) {
                res.json({
                    success : 1,
                    data : result
                })
            }
            else {
                res.json({
                    success : 0,
                    message : "Failed to Insert Data..."
                })
            }
        })
    },

    show_all : (req, res) => {
        userService.getUsers().then(result => {
            if(result) {
                res.json({
                    success : 1,
                    data : result
                })
            }
            else {
                res.json({
                    success : 0
                })
            }
        })
    },

    show_one : (req, res) => {
        const id = req.params.id;
        userService.getUserById(id).then(result => {
            if(result) {
                res.json({
                    success : 1,
                    data : result
                })
            }
            else {
                res.json({
                    success : 0
                })
            }
        })
    },

    update : (req, res) => {
        const id = req.body._id;
        const body = req.body;
        const salt = bcrypt.genSaltSync(10);
        body.password = bcrypt.hashSync(body.password, salt);

        userService.update(id,req.body).then(result => {
            if(result) {
                res.json({
                    success : 1,
                    message : "Data"
                })
            }
            else {
                res.json({
                    success : 0,
                    message : "Failed to Update Data..."
                })
            }
        })
    },

    delete : (req, res) => {
        userService.delete(req.body).then(result => {
            if(result){
                res.json({
                    success : 1,
                    message : "Data",
                    data : result
                })
            }
            else{
                res.json({
                    success : 0,
                    message : "Failed to Delete Data...",
                })
            }
        })
    },

    login : (req, res) => {
        const email = req.body.email;
        const password = req.body.password;
        userService.getUserByUserEmail(email).then(result => {
            if(!result) {
                res.json({
                    success : 0,
                    message : "Invalid Email Or Password"
                })
            }
            
            const r = bcrypt.compareSync(password, result.password);
            if(r) {
                result.password = undefined;
                const jsontoken = jsonwebtoken.sign({ result : result.email}, "rutvik1234", {
                    expiresIn : '1h'
                });
                return res.json({
                    success : 1,
                    message : "Login Successfully",
                    token : jsontoken
                });
            }
            else {
                return res.json({
                    success : 0,
                    userpwd : password,
                    dbpwd : result.password,
                    data : "Invalid email or password",
                    info : result
                });
            }
        });
    }
};